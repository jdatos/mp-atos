<?php
header('Access-Control-Allow-Origin: *');
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");

include '../../essentials/connection.php';

$conn = new Connection();

if(!$conn->connect()) die('Configuration error');
//else echo 'successful connection'; //must delete this upon finalization

$to_decode = json_decode(file_get_contents("php://input"));

$sql = "SELECT ID, username, name, address, contact FROM user_table";
$result = mysqli_query($conn->connect(), $sql);
$results = mysqli_num_rows($result);


$SHOW_TABLES = $to_decode->Action;

echo json_encode(array( $SHOW_TABLES ));


if ($results > 0) {
    while($row = mysqli_fetch_assoc($result)) {
        http_response_code(200);
        echo json_encode(array('ID' =>  $row["ID"] , 'username' =>  $row["username"], 'name' =>  $row["name"], 'address' =>  $row["address"], 'contact' =>  $row["contact"]));
    }
} else {
    http_response_code(403);
    echo json_encode(array('RESPONSE' => 'username not found'));
}




$conn->close($conn->connect());
?>